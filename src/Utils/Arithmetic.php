<?php

namespace App\Utils;

class Arithmetic
{
    public function addiction(int $a, int $b): int
    {
        return $a + $b;
    }

    public function subtraction(int $a, int $b): int
    {
        return $a - $b;
    }

    public function multiplication(int $a, int $b): int
    {
        return $a * $b;
    }

    /**
     * @return float|int
     */
    public function division(int $a, int $b)
    {
        return $a / $b;
    }
}
