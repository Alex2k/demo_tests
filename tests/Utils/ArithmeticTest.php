<?php

use App\Utils\Arithmetic;
use PHPUnit\Framework\TestCase;

class ArithmeticTest extends TestCase
{
    /**
     * @dataProvider additionProvider
     */
    public function testAddition($a, $b, $expected)
    {
        $arithmetic = new Arithmetic();
        $res = $arithmetic->addiction($a, $b);

        $this->assertSame($expected, $res);
    }

    public function additionProvider()
    {
        return [
            [0, 0, 0],
            [0, 1, 1],
            [1, 0, 1],
            [1, 1, 2],
        ];
    }

    /**
     * @dataProvider subtractionProvider
     */
    public function testSubtraction($a, $b, $expected)
    {
        $arithmetic = new Arithmetic();
        $res = $arithmetic->subtraction($a, $b);

        $this->assertSame($expected, $res);
    }

    public function subtractionProvider()
    {
        return [
            [2, 1, 1],
            [0, 1, -1],
            [1, 0, 1],
            [1, 1, 0],
        ];
    }

    /**
     * @dataProvider multiplicationProvider
     */
    public function testMultiplication($a, $b, $expected)
    {
        $arithmetic = new Arithmetic();
        $res = $arithmetic->multiplication($a, $b);

        $this->assertSame($expected, $res);
    }

    public function multiplicationProvider()
    {
        return [
            [0, 0, 0],
            [0, 1, 0],
            [1, 1, 1],
            [2, 2, 4],
        ];
    }

    /**
     * @dataProvider divisionProvider
     */
    public function testDivision($a, $b, $expected)
    {
        $arithmetic = new Arithmetic();
        $res = $arithmetic->division($a, $b);

        $this->assertSame($expected, $res);
    }

    public function divisionProvider()
    {
        return [
            [0, 1, 0],
            [1, 1, 1],
            [2, 2, 1],
            [1, -1, -1],
        ];
    }
}
